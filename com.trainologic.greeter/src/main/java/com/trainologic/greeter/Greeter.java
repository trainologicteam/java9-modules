package com.trainologic.greeter;


/**
 * Created by galmarder on 27/10/2017.
 */
public interface Greeter {


    String greet();

}
