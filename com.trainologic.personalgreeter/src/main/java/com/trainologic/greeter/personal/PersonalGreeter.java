package com.trainologic.greeter.personal;

import com.trainologic.greeter.Greeter;

/**
 * Created by galmarder on 27/10/2017.
 */
public class PersonalGreeter implements Greeter {

    public String greet() {
        return "Hello Shimi";
    }
}
