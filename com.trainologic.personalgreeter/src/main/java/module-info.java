import com.trainologic.greeter.Greeter;
import com.trainologic.greeter.personal.PersonalGreeter;

module com.trainologic.personalgreeter {

    requires com.trainologic.greeter;
    exports com.trainologic.greeter.personal;

    provides Greeter
            with PersonalGreeter;

}