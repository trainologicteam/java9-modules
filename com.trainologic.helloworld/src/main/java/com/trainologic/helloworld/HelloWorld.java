package com.trainologic.helloworld;

import com.trainologic.greeter.Greeter;

import java.util.Optional;
import java.util.ServiceLoader;

/**
 * Created by galmarder on 27/10/2017.
 */
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println(getGreeting().orElse("No Provider") );
    }

    static Optional<String> getGreeting() {
        ServiceLoader<Greeter> sl = ServiceLoader.load(Greeter.class);
        return sl.stream().map(p -> p.get().greet()).reduce((i, g) -> i + " | " + g);
    }
}
