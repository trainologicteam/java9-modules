package com.trainologic.greeter.business;

import com.trainologic.greeter.Greeter;

/**
 * Created by galmarder on 27/10/2017.
 */
public class BusinessGreeter implements Greeter {

    public String greet() {
        return "Hello Trainologic";
    }

}
