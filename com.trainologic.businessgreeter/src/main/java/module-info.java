import com.trainologic.greeter.Greeter;
import com.trainologic.greeter.business.BusinessGreeter;

module com.trainologic.businessgreeter {

    requires com.trainologic.greeter;
    exports com.trainologic.greeter.business;

    provides Greeter
            with BusinessGreeter;

}