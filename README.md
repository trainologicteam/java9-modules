# README #


### What is this repository for? ###

This repo contains examples of using Java 9 modules.
It includes 4 modules:

1. helloworld - the main module which comtains the main method to run.

2. greeter - a module containing the greeter interface.

3. businessgreeter - a module which contains a specific greeter implementation.

4. personalgreeter - another module which contains a specific greeter implementation.


The helloworld module uses the ServiceLoader class to load greeter services from the module path.
The idea was to play with modules a little to get a feeling of the build, packaging, loading adn running of modules together with Maven.

### How do I get set up? ###

The repo is based on maven so either run mvn package to build it or import is as a maven project to your favorite IDE.
To run the example make sure you use -p for instaed of regular classpath in order to use the module-path and not classpath.
Also use -m to run it as a module.
You can see the example in run.sh.
